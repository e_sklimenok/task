<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h1>Общее количество водителей</h1>
                <p>Количество: <xsl:value-of select="count(//person)"/></p>
                <h2>Информация о водителях: </h2>
                <table>
                    <xsl:apply-templates select="//person">
                    </xsl:apply-templates>
                </table>

            </body>
        </html>
    </xsl:template>
    <xsl:template match="person">
        <tr>
            <td><a href="{@name}"><xsl:value-of select="@name"/></a></td>
            <td><xsl:value-of select="@gender"/></td>
            <td><xsl:value-of select="@smoking"/></td>
            <td><xsl:value-of select="@passengers"/></td>
            <td><xsl:value-of select="@category"/></td>
        </tr>

    </xsl:template>
</xsl:stylesheet>